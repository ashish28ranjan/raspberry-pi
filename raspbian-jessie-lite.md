# Raspbian Jessie Lite beginner setup steps
![Raspberry Pi logo](https://upload.wikimedia.org/wikipedia/en/thumb/c/cb/Raspberry_Pi_Logo.svg/188px-Raspberry_Pi_Logo.svg.png)

### Enable WiFi
1. ```sudo nano /etc/wpa_supplicant/wpa_supplicant.conf```
2. Add
```
network={
    ssid="YOUR_SSID"
    psk="YOUR_PASSWORD"
}
```
3. ```sudo reboot```


### Assign static IP on WiFi for ssh
1. ```sudo nano /etc/dhcpcd.conf```
2.  Add the following to the bottom of this file
```
interface wlan0
static ip_address=192.168.0.120/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1 8.8.8.8
```
3. ```sudo reboot```

##### Older versions:

1. ```sudo nano /etc/network/interfaces```
2. Run ```ifconfig``` and notedown the follwing for wlan0
    inet addr:192.168.1.81  Bcast:192.168.1.255  Mask:255.255.255.0

3. Run ```route -n``` and notedown the following
'Gateway' Address – 192.168.1.254
'Destination' Address – 192.168.1.0

4. Replace
```iface wlan0 inet manual``` with
```iface wlan0 inet static```
5. Add the following below the above line
```
    address 192.168.1.120
    netmask 255.255.255.0
    network 192.168.1.0
    broadcast 192.168.1.255
    gateway 192.168.1.1
```
6. Save this file
7. ```sudo reboot```


### Set global aliases
1. ```sudo -i```
2. ```echo "alias ll='ls -la --color=auto'" >> /etc/profile.d/global_alias.sh```
3. ```logout```


### Change font-size of terminal
1. ```sudo nano /etc/default/console-setup```
2. Update with following entries
```
CODESET="Uni2"
FONTFACE="TerminusBold"
FONTSIZE="16x32"
```
3. ```sudo service console-setup restart```


### Update & upgrade
```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get clean
sudo apt-get install piclone geany usb-modeswitch pi-bluetooth
```

### Change pi's configs
Source: https://www.raspberrypi.org/forums/viewtopic.php?f=66&t=133691
##### 1. Mandatory
1. ```sudo raspi-config```
2. Update this tool
3. Change password
4. Change username
5. Set timezone & wifi-country & locale
6. Enable ssh

##### 2. Non-mandatory
1. Enable/Disable VNC for GUI access
    ```sudo apt-get install lightdm```

### Remot3 it
Source: https://www.raspberrypi.org/documentation/remote-access/access-over-Internet/
1. ```sudo apt-get update```
2. ```sudo apt-get install weavedconnectd```
3. ```sudo weavedinstaller```

### Install Miniconda3
Source: http://stackoverflow.com/questions/39371772/how-to-install-anaconda-on-raspberry-pi-3-model-b

1. Get the latest version of miniconda for Raspberry pi - made for armv7l processor and bundled with Python 3 (eg.: uname -m)
```sh
wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-armv7l.sh
sudo md5sum Miniconda3-latest-Linux-armv7l.sh
sudo /bin/bash Miniconda3-latest-Linux-armv7l.sh
```

2. When installing change the default dir installation from /root/miniconda3 to: ```/home/pi/miniconda3```

3. Edit the .bashrc file
```sudo nano /home/pi/.bashrc```
...and add the following line to the end of the file
```export PATH="/home/pi/miniconda3/bin:$PATH"```

4. Save and close this file & reboot Raspberry pi
```sudo reboot -h now```

5. After reboot enter the following command "python --version" which should give you:
```Python 3.4.3 :: Continuum Analytics, Inc.```

### Install pip
1. ```sudo apt install python-pip	#python 2```
```sudo apt install python3-pip	#python 3```

Or,
1. ```sudo /home/pi/miniconda3/bin/conda install pip```
2. ```sudo /home/pi/miniconda3/bin/pip install --upgrade pip```

### Install virtualenv
1. ```virtualenv --python=/usr/bin/python3 <path/to/new/virtualenv/>```


### Conda
#### Add additional envs directory to search
```conda config --add envs_dirs /home/pi/.conda/envs```


### Install Matplotlib
##### 1. Build it manually (preferred)
```sh
git clone git://github.com/matplotlib/matplotlib.git
cd matplotlib
python setup.py install
```

##### 2. Using prebuilt packages
```python -m pip install matplotlib```

### Install pyspeedtest
```pip install pyspeedtest```

### Install TwitterAPI
```pip install TwitterAPI```

### Backup current image
1. Mac
```diskutil list```
```sudo dd bs=4m if=/dev/rdisk2 of=raspbian.img```
2. Linux
```sudo dd bs=4M if=/dev/sdb of=raspbian.img```

### Bluetooth
Source 1: https://www.raspberrypi.org/forums/viewtopic.php?t=68779
Source 2: https://gist.github.com/oleq/24e09112b07464acbda1

### Add GUI Desktop environment
Source: https://www.raspberrypi.org/forums/viewtopic.php?f=66&t=133691

### Magic Mirror (Jessie Lite)
Source: https://github.com/MichMich/MagicMirror/wiki/Jessie-Lite-Installation-Guide

### Set up Network Speed Bot
```sh
git clone https://gitlab.com/ashish28ranjan/network-speed-bot.git
cd network-speed-bot
touch debug.log
sudo chmod 666 debug.log
touch internet_speed.json
sudo chmod 666 internet_speed.json
```

### Search python from process
```ps -ef | grep python```

